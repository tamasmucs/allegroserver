from flask import Flask
from flask import request
from youtube_dl import YoutubeDL
import vlc
import glob
import shutil 
import os 
import json

app = Flask(__name__)
player = vlc.MediaPlayer()
youtube_filename = None

def my_hook(d):
    global youtube_filename
    youtube_filename = d['filename']

ydl_opts = {
    'format': 'bestaudio/best',
    'progress_hooks': [my_hook]
}

downloader = YoutubeDL(ydl_opts)


@app.route("/", methods = ['GET'])
def report_status():
    status = {
                "is_playing" : player.is_playing()
                 }
    return str(status)

    
@app.route("/allegro/playerinfo/", methods =['PUT','DELETE'])
def player_handler():
    global player
    req_data = json.loads(request.data)

    if request.method == 'DELETE':
        player.stop()
        return app.make_response(('','204',[]))
    elif request.method == 'PUT':
        # update volume, if volume data is in request
        if "vol" in req_data:
            
            current_volume = player.audio_get_volume()
            if req_data["vol"] == "+":
                if  current_volume+5 <= 100:
                    player.audio_set_volume(current_volume+5)
                    print "volume set to "+str(player.audio_get_volume())
            elif req_data["vol"] == "-":
                if  current_volume-5 >= 0:
                    player.audio_set_volume(current_volume-5)
                    print "volume set to "+str(player.audio_get_volume())
            else:
                raise Exception("Invalid volume data: ", req_data["vol"])
            
        
        return app.make_response(('','204',[]))
    else:
        abort(501)
    
    
@app.route("/allegro/youtubehandler/", methods =['PUT'])
def youtube_handler():
    global player
    # parse JSON
    req_data = json.loads(request.data)

    if request.method == 'PUT':
        # download the file
        downloader.download([req_data['url']])
        # stop!
        # player.stop()
        # find the file
        filename = youtube_filename
        
        # set new mrl
        player.set_mrl(filename)
        
        # play
        player.play()
        return app.make_response(('','204',[]))
    else:
        abort(501)
    

@app.route("/allegro/radio/", methods =['PUT'])
def radio_handler():
    global player
    
    req_data = json.loads(request.data)

    if request.method == 'PUT':
        player.stop()
        
        # play url sent along with the HTTP request
        player.set_mrl(req_data['url'])
        
        player.play()
        
        return app.make_response(('','204',[]))
    else:
        # HTTP verb unsupported
        abort(501)
    
        
if __name__ == "__main__":
    app.run(host='0.0.0.0',
                debug=True)